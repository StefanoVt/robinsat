Robin SAT Solver
================
This solver iterates over the F_i subproblems in a round robin fashion.

Compilation
-----------
the only external dependency is the gflags library, that is used to process command
line flags. A version of minisat is provided in the lib subdirectory. The project
uses cmake as a build tool; in order to compile it is sufficient to do:
```
mkdir build; cd build
cmake ..
make
```

Execution
---------
Training sets are preprocessed using the python scripts contained in the scripts
folder. The scripts generate an index file to be fed to the c++ executable.

### Default execution ###
In the default case, the robin solver loads all subproblems in memory and enable
or disable them using fresh variables. The decommon.py script preprocess the `F_i`
files: it reindex each `y_i` in order to avoid clashes and creates the additional
fresh variables. Starting from the compressed files in the data directory, these
are the necessary commands:
```
mkdir example; cd example
tar xf ../multiple-cnf.tar.gz
python2 make_train.py
../../scripts/decommon.py 182 new*.cnf
../../build/robinsat index.txt ../empty.cnf
```

instead of empty.cnf one could add any constraints on the `x` obtained by MUS
enumeration.

### SimpSolver variant ###
This variant attempts to avoid to load the whole problem in memory by exploiting
the simplification capabilities of minisat. It is a workaround on the lack of
push/pop primitives on the original minisat, and it is still bugged. The way to
execute it is similar:
```
mkdir example; cd example
tar xf ../multiple-cnf.tar.gz
python2 make_train.py
../../scripts/decommon.py 182 new*.cnf
../../build/robinsat -type simp index.txt ../empty.cnf
```
As said before, this version is still buggy and will crash.

### Assumptions variant ###
This variants exploits the structure of the subproblem `F_i(x,y) = F(x,y,TP(i))`
by loading the single `F(x,y,z)` in memory and using the "solve under assumptions"
primitive to select the subproblem. This version does not need the generation of
the different subproblems and the following reindexing, so the format of the
execution commands is a bit different:
```
mkdir example; cd example
tar xf ../multiple-cnf.tar.gz
../../scripts/make_index.py
../../build/robinsat -type asmp indexasmp.txt master-file.cnf
```
