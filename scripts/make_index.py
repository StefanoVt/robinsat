#!/usr/bin/env python2
import glob

files = list(glob.glob("training-point-*.cnf"))

with open("indexasmp.txt", 'w') as f:
    print >>f, len(files)
    for fil in files:
        print >>f, fil, 0
