#!/usr/bin/env python2
'''change variable id of different cnf files, so that only common variables share
the same variable id'''

import sys
import os.path as path

max_common = int(sys.argv[1])
cnf_files = sys.argv[2:]

def change_fname(fn):
    return fn.replace(".cnf", "_alt.cnf")

def parse_cnf(f):
    header = f.readline()
    assert header.startswith('p cnf ')
    _,_,numvars, numclauses = header.strip().split()
    data = []
    for clause in f:
        c = map(int, clause.strip().split())[:-1]
        assert(len(c) > 0)
        data.append(c)
    return int(numvars), data


def save_cnf(newfn, newclauses, newnumvars):
    with(open(newfn, 'w')) as f:
        print >>f, "p cnf", newnumvars, len(newclauses)
        for c in newclauses:
            print >>f, ' '.join(map(str,c)), 0



def sign(x):
    return x/abs(x)

indexdata = []
shift = 0
for fn in cnf_files:
    with open(fn) as f:
        numvars, clauses = parse_cnf(f)

        def shiftClause(clause):
            return [(x if abs(x) <= max_common else x+(shift*sign(x))) for x in clause]

        def shiftClause2(clause):
            return [-newnumvars]+ shiftClause(clause)
        newnumvars = numvars + shift + 1
        newclauses = map(shiftClause2, clauses)
        newfn = change_fname(fn)
        save_cnf(newfn, newclauses,newnumvars)
        indexdata.append((newfn, newnumvars))

        if numvars > max_common:
            shift += numvars - max_common + 1

with open("index.txt", "w") as f:
    print >>f, len(indexdata)
    for fn, i in indexdata:
        print >>f, fn, i
