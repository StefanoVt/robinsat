#include <stdio.h>
#include <stdlib.h>
#include "RobinSimpSolver.hh"
using namespace std;
using namespace Minisat;

RobinSimpSolver::RobinSimpSolver(std::vector<string> &files, vector<int> &index)
    : partToVar(index), s(nullptr), files(files), end(index.size()) {
  for (int i = 0; i < partToVar.size(); i++) {
    partToVar[i]--;
  }
  newSolver();
}

void RobinSimpSolver::newSolver() {
  s.reset(new SimpSolver());
  s->verbosity = 999;
  for (size_t i = 0; i < shared; i++) {
    s->freezeVar(s->newVar());
  }
}

void RobinSimpSolver::preloadFile(std::string &file) {
  parse_DIMACS(gzopen(file.c_str(), "r"), *s);
}

vector<bool> &RobinSimpSolver::solve() {
  Minisat::vec<Minisat::Lit> assumptions;

  for (auto i = assmp.cbegin(); i != assmp.cend(); i++) {
    assumptions.push(i->second);
  }
  solution.clear();

  if (!s->solve(assumptions))
    return solution;
  cout << "simplified size:" << s->merges << " " << s->asymm_lits << " "
       << s->eliminated_vars << endl;
  for (int i = 0; i < shared; i++) {
    solution.push_back(s->model[i] == l_True);
  }
  return solution;
}

void RobinSimpSolver::push(vector<bool> &what) {
  for (int i = 0; i < what.size(); i++) {
    assmp.erase(i);
    assmp.emplace(i, mkLit(i, !what[i]));
  }
}

void RobinSimpSolver::push(int i) {
  parse_DIMACS(gzopen(files[i].c_str(), "r"), *s);
  assmp.erase(partToVar[i]);
  assmp.emplace(partToVar[i], mkLit(partToVar[i]));
}

void RobinSimpSolver::pop(std::vector<bool> &v) {
  for (int i = 0; i < v.size(); i++) {
    assmp.erase(i);
  }
}

void RobinSimpSolver::pop(int i) {
  assmp.erase(partToVar[i]);
  s->releaseVar(mkLit(partToVar[i], true));
  s->eliminate();
}

void RobinSimpSolver::add_mus(const vector<bool> &mu) {
  auto &conflict = s->conflict;
  vec<Lit> newclause;
  for (int i = 0; i < conflict.size(); i++) {
    if (var(conflict[i]) < mu.size()) {
      newclause.push(conflict[i]);
    }
  }
  if (!newclause.size()) {
    printvector(mu);
    printvec(conflict.toVec());
    abort();
  }
  // newclause[newclause.size() - 1] = ~newclause.last();
  cout << "adding clause: ";
  printvec(newclause);
  s->addClause(newclause);
}

void RobinSimpSolver::resetSolver() {
  char *ptr;
  size_t size;
  FILE *ss = open_memstream(&ptr, &size);
  vec<Lit> empty;
  vec<Var> simplemap;
  for (Var i = 0; i < shared; i++) {
    simplemap.push(i);
  }
  fprintf(ss, "p cnf %d %d\n", shared, s->nClauses() + s->nAssigns());
  for (auto i = s->clausesBegin(); i != s->clausesEnd(); i.operator++()) {
    s->toDimacs(ss, const_cast<Clause &>(*i), simplemap, (Var &)shared);
  }
  for (auto i = s->trailBegin(); i != s->trailEnd(); i.operator++()) {
    fprintf(ss, "%d 0\n", sign(*i) ? -var(*i) - 1 : var(*i) + 1);
  }
  fprintf(ss, "%c", EOF);
  fclose(ss);
  newSolver();
  parse_DIMACS_main(ptr, *s);
}

std::vector<bool> &RobinSimpSolver::findSolution() {
  int i = 0, j, start = 0;
  std::vector<int> v;
  while (true) {
    j = start;
    push(j);
    auto mu_0 = solve();
    pop(j);
    if (mu_0.empty())
      return solution;
    i = 1;
    push(mu_0);
    while (true) {
      j = (i + start) % end;
      push(j);
      if (!solve().empty()) {
        i++;
        if (i == end)
          return solution;
        else {
          pop(j);
        }
      } else {
        printvector(mu_0);
        cout << "failed at attempt " << i << endl;
        pop(j);
        pop(mu_0);
        add_mus(mu_0);
        resetSolver();
        start = (i + start) % end;
        i = 0;
        break;
      }
    }
  }
}
