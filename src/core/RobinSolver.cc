#include "RobinSolver.hh"
using namespace std;
using namespace Minisat;

RobinSolver::RobinSolver(std::vector<string> &files, vector<int> &index)
    : partToVar(index), end(index.size()) {
  for (size_t i = 0; i < 182; i++) {
    s.freezeVar(s.newVar());
  }
  for (int i = 0; i < partToVar.size(); i++) {
    partToVar[i]--;
    while (s.nVars() <= partToVar[i])
      s.newVar();
    s.freezeVar(partToVar[i]);
  }
  loadFiles(files);
  s.verbosity = 999;
}

void RobinSolver::loadFiles(std::vector<string> &files) {
  // load all files into solver, save partToVar map
  for (auto i = files.begin(); i != files.end(); i++) {
    parse_DIMACS(gzopen(i->c_str(), "r"), s);
  }
  for (int i = 0; i < partToVar.size(); i++) {
    assmp.emplace(partToVar[i], mkLit(partToVar[i], true));
    s.addClause(mkLit(partToVar[i]));
  }
}

void RobinSolver::preloadFile(std::string &file) {
  parse_DIMACS(gzopen(file.c_str(), "r"), s);
}

vector<bool> &RobinSolver::solve() {
  Minisat::vec<Minisat::Lit> assumptions;

  for (auto i = assmp.cbegin(); i != assmp.cend(); i++) {
    assumptions.push(i->second);
  }
  solution.clear();

  if (!s.solve(assumptions))
    return solution;
  for (int i = 0; i < 182; i++) {
    solution.push_back(s.model[i] == l_True);
  }
  return solution;
}

void RobinSolver::push(vector<bool> &what) {
  for (int i = 0; i < what.size(); i++) {
    assmp.erase(i);
    assmp.emplace(i, mkLit(i, !what[i]));
  }
}

void RobinSolver::push(int i) {
  assmp.erase(i);
  assmp.emplace(i, mkLit(i));
}

void RobinSolver::pop(std::vector<bool> &v) {
  for (int i = 0; i < v.size(); i++) {
    assmp.erase(i);
  }
}

void RobinSolver::pop(int i) {
  assmp.erase(i);
  assmp.emplace(i, mkLit(i, true));
}

void RobinSolver::add_mus(const vector<bool> &mu) {
  auto &conflict = s.conflict;
  vec<Lit> newclause;
  for (int i = 0; i < conflict.size(); i++) {
    if (var(conflict[i]) < mu.size()) {
      newclause.push(conflict[i]);
    }
  }
  if (!newclause.size()) {
    printvector(mu);
    printvec(s.conflict.toVec());
    abort();
  }
  // newclause[newclause.size() - 1] = ~newclause.last();
  cout << "adding clause: ";
  printvec(newclause);
  s.addClause(newclause);
}

std::vector<bool> &RobinSolver::findSolution() {
  int i = 0, j, start = 0;
  std::vector<int> v;
  while (true) {
    j = partToVar[start];
    push(j);
    auto mu_0 = solve();
    pop(j);
    if (mu_0.empty())
      return solution;
    i = 1;
    push(mu_0);
    while (true) {
      j = partToVar[(i + start) % end];
      push(j);
      if (!solve().empty()) {
        i++;
        if (i == end)
          return solution;
        else {
          pop(j);
        }
      } else {
        printvector(mu_0);
        cout << "failed at attempt " << i << endl;
        pop(j);
        pop(mu_0);
        add_mus(mu_0);
        start = (i + start) % end;
        i = 0;
        break;
      }
    }
  }
}
