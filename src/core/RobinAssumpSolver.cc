#include <stdio.h>
#include <stdlib.h>
#include "RobinAssumpSolver.hh"
using namespace std;
using namespace Minisat;

void RobinAssumpSolver::parseFiles(const std::vector<string> files) {
  for (size_t i = 0; i < files.size(); i++) {
    ifstream f(files[i]);
    string p, cnf;
    int nvars, nclauses;
    vec<Lit> *entry = (new vec<Lit>);
    vec<Lit> &assumpVec = *entry;
    f >> p >> cnf >> nvars >> nclauses;
    while (!f.eof()) {
      int assump, zero;
      f >> assump >> zero;
      assert(assump != 0 && zero == 0);
      assumpVec.push(mkLit(abs(assump) - 1, assump < 0));
    }
    assumpMap.insert(make_pair(i, unique_ptr<vec<Lit>>(entry)));
  }
}

RobinAssumpSolver::RobinAssumpSolver(std::vector<string> &files,
                                     vector<int> &index)
    : s(), end(index.size()) {
  for (size_t i = 0; i < shared; i++) {
    s.freezeVar(s.newVar());
  }
  parseFiles(files);
  auto &exMap = *assumpMap[0];
  for (size_t i = 0; i < exMap.size(); i++) {
    auto v = var(exMap[i]);
    while (s.nVars() <= v)
      s.newVar();
    s.freezeVar(v);
  }
}

void RobinAssumpSolver::preloadFile(std::string &file) {
  parse_DIMACS(gzopen(file.c_str(), "r"), s);
}

vector<bool> &RobinAssumpSolver::solve() {
  Minisat::vec<Minisat::Lit> assumptions;

  if (zindex >= 0) {
    assumpMap[zindex]->copyTo(assumptions);
  }

  for (auto i = assmp.cbegin(); i != assmp.cend(); i++) {
    assumptions.push(i->second);
  }
  solution.clear();

  if (!s.solve(assumptions))
    return solution;
  for (int i = 0; i < shared; i++) {
    solution.push_back(s.model[i] == l_True);
  }
  return solution;
}

void RobinAssumpSolver::push(vector<bool> &what) {
  for (int i = 0; i < what.size(); i++) {
    assmp.erase(i);
    assmp.emplace(i, mkLit(i, !what[i]));
  }
}

void RobinAssumpSolver::push(int i) { zindex = i; }

void RobinAssumpSolver::pop(std::vector<bool> &v) {
  for (int i = 0; i < v.size(); i++) {
    assmp.erase(i);
  }
}

void RobinAssumpSolver::pop(int i) { zindex = -1; }

/*
#include "temp.hh"
void RobinAssumpSolver::shrinkMus(vec<Lit> &mus) {
  vec<Lit> index;
  for (int i = 0; i < mus.size(); i++) {
    index.push(mkLit(i, true));
  }
  do {
    vec<Lit> temp, temp2;
    assumpMap[zindex]->copyTo(temp);
    for (size_t i = 0; i < index.size(); i++) {
      if (!sign(index[i])) {
        temp.push(~mus[i]);
        temp2.push(mus[i]);
      }
    }
    if (!s.solve(temp)) {
      temp2.moveTo(mus);
      return;
    }
  } while (gosperHack(index, index));
}
*/

void RobinAssumpSolver::shrinkMus(vec<Lit> &mus) {
  Minisat::vec<Minisat::Lit> assumptions;
  if (zindex >= 0) {
    assumpMap[zindex]->copyTo(assumptions);
  }
  int firstmus = assumptions.size();
  for (size_t j = 0; j < mus.size(); j++) {
    assumptions.push(~mus[j]);
  }

  for (int i = 0; i < mus.size(); i++) {
    assert(mus.size() == assumptions.size() - firstmus);
    swap(mus[i], mus[mus.size() - 1]);
    swap(assumptions[i + firstmus], assumptions[assumptions.size() - 1]);
    auto temp = assumptions.last();
    assumptions.pop();
    assumptions.push(mus[mus.size() - 1]);
    bool result = s.solve(assumptions);
    assumptions.pop();
    if (!result) {
      mus.pop();
      auto &confl = s.conflict.toVec();
      for (int j = i; j < mus.size(); j++) {
        bool shortcut = true;
        for (int k = 0; k < confl.size(); k++) {
          shortcut = shortcut && confl[k] != mus[j];
        }
        if (shortcut) {
          swap(mus[j], mus[mus.size() - 1]);
          swap(assumptions[j + firstmus], assumptions[assumptions.size() - 1]);
          mus.pop();
          assumptions.pop();
        }
      }
      i--;
    } else {
      assumptions.push(temp);
      swap(mus[i], mus[mus.size() - 1]);
      swap(assumptions[i + firstmus], assumptions[assumptions.size() - 1]);
    }
    // assert(!s.solve(assumptions));
  }
}

void RobinAssumpSolver::add_mus(const vector<bool> &mu) {
  auto &conflict = s.conflict;
  vec<Lit> newclause;
  for (int i = 0; i < conflict.size(); i++) {
    if (var(conflict[i]) < mu.size()) {
      newclause.push(conflict[i]);
    }
  }
  // printvec(conflict.toVec());
  shrinkMus(newclause);
  if (!newclause.size()) {
    printvector(mu);
    printvec(conflict.toVec());
    printvec(newclause);
    abort();
  }
  // newclause[newclause.size() - 1] = ~newclause.last();
  cout << "adding clause: ";
  printvec(newclause);
  s.addClause(newclause);
}

std::vector<bool> &RobinAssumpSolver::findSolution() {
  int i = 0, j, start = 0;
  std::vector<int> v;
  while (true) {
    push(start);
    auto mu_0 = solve();
    pop(start);
    if (mu_0.empty())
      return solution;
    i = 1;
    push(mu_0);
    while (true) {
      j = (i + start) % end;
      push(j);
      if (!solve().empty()) {
        i++;
        if (i == end)
          return solution;
        else {
          pop(j);
        }
      } else {
        printvector(mu_0);
        cout << "failed at attempt " << i << endl;
        add_mus(mu_0);
        pop(j);
        pop(mu_0);
        start = (i + start) % end;
        i = 0;
        break;
      }
    }
  }
}
