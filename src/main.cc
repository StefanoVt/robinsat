#include "RobinSimpSolver.hh"
#include "RobinSolver.hh"
#include "RobinAssumpSolver.hh"
#include "FlipAssumpSolver.hh"
#include <gflags/gflags.h>
using namespace std;

DEFINE_string(type, "std,simp", "type of solver used");

int main(int argc, char *argv[]) {
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  assert(argc > 2);
  ifstream indexf(argv[1]);
  int nparts;
  indexf >> nparts;
  std::vector<string> files;
  std::vector<int> indexes;
  for (size_t i = 0; i < nparts; i++) {
    string fn;
    int indx;
    indexf >> fn >> indx;
    files.push_back(fn);
    indexes.push_back(indx);
  }
  AbstractRobinSolver *rs;
  if (FLAGS_type == "simp") {
    rs = new RobinSimpSolver(files, indexes);
  } else if (FLAGS_type == "asmp") {
    rs = new RobinAssumpSolver(files, indexes);
  } else if (FLAGS_type == "flip") {
    rs = new FlipAssumpSolver(files, indexes);
  } else {
    rs = new RobinSolver(files, indexes);
  }
  string pf(argv[2]);
  rs->preloadFile(pf);
  auto &a = rs->findSolution();
  printCnf(a);
}
