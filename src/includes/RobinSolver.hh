#ifndef ROBINSOLVER_HH
#define ROBINSOLVER_HH
#include "minisat/simp/SimpSolver.h"
#include "AbstractRobinSolver.hh"
class RobinSolver : public AbstractRobinSolver {
  std::vector<int> partToVar;
  Minisat::SimpSolver s;
  std::vector<bool> solution;
  std::map<int, Minisat::Lit> assmp;
  void add_mus(const std::vector<bool> &mu);
  int end;

public:
  RobinSolver(std::vector<std::string> &folder, std::vector<int> &v);
  void loadFiles(std::vector<std::string> &folder);
  void preloadFile(std::string &file);
  std::vector<bool> &findSolution();
  std::vector<bool> &solve();
  void push(std::vector<bool> &what);
  void push(int part);
  void pop(std::vector<bool> &v);
  void pop(int part);
};
#endif
