#ifndef FlipAssumpSolver_HH
#define FlipAssumpSolver_HH
#include <memory>
#include "AbstractRobinSolver.hh"
#include "minisat/simp/SimpSolver.h"
class FlipAssumpSolver : public AbstractRobinSolver {
  Minisat::SimpSolver s;
  std::map<int, Minisat::Lit> assmp;
  std::vector<bool> solution;
  std::map<int, std::unique_ptr<Minisat::vec<Minisat::Lit>>> assumpMap;
  int zindex = -1;
  int shared = 182, end;
  void add_mus(std::vector<bool> &mu);
  void parseFiles(const std::vector<std::string> files);
  void shrinkMus(Minisat::vec<Minisat::Lit> &m, std::vector<bool> &mu);

public:
  FlipAssumpSolver(std::vector<std::string> &folder, std::vector<int> &v);
  void preloadFile(std::string &file);
  std::vector<bool> &findSolution();
  std::vector<bool> &solve();
  void push(std::vector<bool> &what);
  void push(int part);
  void pop(std::vector<bool> &v);
  void pop(int part);
};
#endif
