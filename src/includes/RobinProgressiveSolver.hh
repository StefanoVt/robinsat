#ifndef RobinProgressiveSolver_HH
#define RobinProgressiveSolver_HH
#include <memory>
#include "AbstractRobinSolver.hh"
#include "minisat/simp/SimpSolver.h"
class RobinProgressiveSolver : public AbstractRobinSolver {
  Minisat::SimpSolver s;
  std::map<int, Minisat::Lit> assmp;
  std::vector<bool> solution;
  std::map<int, std::unique_ptr<Minisat::vec<Minisat::Lit>>> assumpMap;
  int zindex = -1;
  int shared = 182, end;
  int add_mus(const std::vector<bool> &mu);
  void parseFiles(const std::vector<std::string> files);
  void shrinkMus(Minisat::vec<Minisat::Lit> &m);

public:
  RobinProgressiveSolver(std::vector<std::string> &folder, std::vector<int> &v);
  void preloadFile(std::string &file);
  std::vector<bool> &findSolution();
  std::vector<bool> &solve();
  void push(std::vector<bool> &what);
  void push(int part);
  void pop(std::vector<bool> &v);
  void pop(int part);
};
#endif
