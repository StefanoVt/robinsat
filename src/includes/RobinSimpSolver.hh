#ifndef ROBINSIMPSOLVER_HH
#define ROBINSIMPSOLVER_HH
#include <memory>
#include "minisat/simp/SimpSolver.h"
#include "AbstractRobinSolver.hh"
class RobinSimpSolver : public AbstractRobinSolver {
  std::vector<int> partToVar;
  std::unique_ptr<Minisat::SimpSolver> s;
  std::vector<bool> solution;
  std::map<int, Minisat::Lit> assmp;
  std::vector<std::string> files;
  int shared = 182;
  void add_mus(const std::vector<bool> &mu);
  int end;
  void resetSolver();
  void newSolver();

public:
  RobinSimpSolver(std::vector<std::string> &folder, std::vector<int> &v);
  void preloadFile(std::string &file);
  std::vector<bool> &findSolution();
  std::vector<bool> &solve();
  void push(std::vector<bool> &what);
  void push(int part);
  void pop(std::vector<bool> &v);
  void pop(int part);
};
#endif
