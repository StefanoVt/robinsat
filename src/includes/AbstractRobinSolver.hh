#ifndef ABSTRACTROBINSOLVER_HH
#define ABSTRACTROBINSOLVER_HH
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include "minisat/core/Solver.h"
#include "minisat/core/Dimacs.h"
class AbstractRobinSolver {
public:
  virtual void preloadFile(std::string &file) = 0;
  virtual std::vector<bool> &findSolution() = 0;
  /*std::vector<bool> &solve()=0;
  void push(std::vector<bool> &what);
  void push(int part);
  void pop(std::vector<bool> &v);
  void pop(int part);*/
};

inline void printvector(const std::vector<bool> &v) {
  for (int i = 0; i < v.size(); i++) {
    std::cout << (v[i] ? "+" : "-") << "";
  }
  std::cout << std::endl;
}
inline void printvec(const Minisat::vec<Minisat::Lit> &v) {
  for (int i = 0; i < v.size(); i++) {
    std::cout << (sign(v[i]) ? "-" : "") << var(v[i]) + 1 << " ";
  }
  std::cout << std::endl;
}

inline void printIdx(const std::vector<bool> &v) {
  for (int i = 0; i < v.size(); i++) {
    std::cout << (v[i] ? "" : "-") << i + 1 << " ";
  }
  std::cout << std::endl;
}

inline void printCnf(const std::vector<bool> &v) {
  std::cout << "p cnf " << v.size() << " " << v.size() << std::endl;
  for (int i = 0; i < v.size(); i++) {
    std::cout << (v[i] ? "" : "-") << i + 1 << " 0" << std::endl;
  }
}

#endif
