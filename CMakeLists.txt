cmake_minimum_required(VERSION 3.4)
project(robinsat)
#set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")
set(CMAKE_CXX_STANDARD 11)
find_package(gflags)
#find_package(Gperftools)
file(GLOB main_SRC
    "src/core/*.cc"
)
file(GLOB test_SRC
    "tests/units/*.cc"
)
include_directories(lib/catch src/includes lib/minisat/)
add_subdirectory(lib/minisat)
enable_testing(true)
add_executable(robinsat src/main.cc ${main_SRC})
target_link_libraries(robinsat minisat-lib-static gflags ${GPERFTOOLS_LIBRARIES})
add_executable(robinsat-test tests/main.cc ${test_SRC} ${main_SRC})
target_link_libraries(robinsat-test minisat-lib-static ${GPERFTOOLS_LIBRARIES})
add_test(unit robinsat-test)
